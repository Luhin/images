﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;


namespace Images
{

    public partial class MainWindow : Window
    {

        private WriteableBitmap bitmap;

        public MainWindow()
        {
            InitializeComponent();

            Butt.Click += ButtonClick;
        }

        public void ButtonClick(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();

            string[] list = Directory.GetFiles(dialog.SelectedPath);

            FillImageList(list);
        }

        public void FillImageList(string[] list)
        {
            ImgCnt.Items.Clear();

            foreach (string s in list)
            {
                if (s.EndsWith(".jpg") || s.EndsWith(".jpeg"))
                {
                    ImgCnt.Items.Add(new MyImage(s));
                }
            }
        }

        public void SelectImage(object sender, RoutedEventArgs e)
        {
            MyImage image = (MyImage)ImgCnt.SelectedItem;
            if (image != null)
            {
                BitmapImage bitImg = new BitmapImage(new Uri(image.FullPath, UriKind.Absolute));
                bitmap = new WriteableBitmap(bitImg);
                MainImage.Source = bitmap;
            }
        }
    }
    public class MyImage
    {
        private string name;
        private string fullPath;

        public string Name { get => name; set => name = value; }
        public string FullPath { get => fullPath; set => fullPath = value; }

        public MyImage(string path)
        {
            this.FullPath = path;
            this.Name = System.IO.Path.GetFileName(FullPath);
        }
    }
}
